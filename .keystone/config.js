"use strict";
var __defProp = Object.defineProperty;
var __getOwnPropDesc = Object.getOwnPropertyDescriptor;
var __getOwnPropNames = Object.getOwnPropertyNames;
var __hasOwnProp = Object.prototype.hasOwnProperty;
var __export = (target, all) => {
  for (var name in all)
    __defProp(target, name, { get: all[name], enumerable: true });
};
var __copyProps = (to, from, except, desc) => {
  if (from && typeof from === "object" || typeof from === "function") {
    for (let key of __getOwnPropNames(from))
      if (!__hasOwnProp.call(to, key) && key !== except)
        __defProp(to, key, { get: () => from[key], enumerable: !(desc = __getOwnPropDesc(from, key)) || desc.enumerable });
  }
  return to;
};
var __toCommonJS = (mod) => __copyProps(__defProp({}, "__esModule", { value: true }), mod);

// keystone.ts
var keystone_exports = {};
__export(keystone_exports, {
  default: () => keystone_default
});
module.exports = __toCommonJS(keystone_exports);
var import_core2 = require("@keystone-6/core");

// schema.ts
var import_core = require("@keystone-6/core");
var import_fields = require("@keystone-6/core/fields");
var filterPost = ({ session: session2 }) => {
  if (session2?.data.isAdmin)
    return true;
  return { isPublished: { equals: true } };
};
var isAdmin = ({ session: session2, context, listKey, operation }) => session2?.data.isAdmin;
var lists = {
  User: (0, import_core.list)({
    access: {
      operation: {
        query: ({ session: session2, context, listKey, operation }) => true,
        create: isAdmin,
        update: isAdmin,
        delete: isAdmin
      }
    },
    fields: {
      name: (0, import_fields.text)({
        validation: { isRequired: true },
        isIndexed: "unique",
        hooks: {
          validateInput: ({ addValidationError, resolvedData, fieldKey }) => {
            const name = resolvedData[fieldKey];
            if (name !== void 0 && name !== null && !name.toString().includes("@")) {
              addValidationError(`The name address ${name} provided for the field ${fieldKey} must contain an '@' character`);
            }
          }
        }
      }),
      password: (0, import_fields.password)(),
      age: (0, import_fields.integer)({ validation: { isRequired: true }, defaultValue: 18 }),
      post: (0, import_fields.relationship)({ ref: "Post.author", many: false }),
      isAdmin: (0, import_fields.checkbox)()
    }
  }),
  Post: (0, import_core.list)({
    access: {
      operation: {
        query: ({ session: session2, context, listKey, operation }) => true,
        create: isAdmin,
        update: isAdmin,
        delete: isAdmin
      },
      filter: {
        query: filterPost
      }
    },
    hooks: {
      validateInput: ({ resolvedData, addValidationError }) => {
        const { author } = resolvedData;
        if (!author) {
          addValidationError("The author of a blog post cannot be the empty ");
        }
      }
    },
    fields: {
      title: (0, import_fields.text)({ validation: { isRequired: true } }),
      isPublished: (0, import_fields.checkbox)(),
      publishDate: (0, import_fields.timestamp)(),
      author: (0, import_fields.relationship)({ ref: "User.post", many: false })
    }
  })
};

// keystone.ts
var import_auth = require("@keystone-6/auth");
var import_session = require("@keystone-6/core/session");
var { withAuth } = (0, import_auth.createAuth)({
  listKey: "User",
  identityField: "name",
  secretField: "password",
  sessionData: "isAdmin",
  initFirstItem: {
    fields: ["name", "password", "age", "post", "isAdmin"]
  }
});
var session = (0, import_session.statelessSessions)({
  secret: "-- EXAMPLE COOKIE SECRET; CHANGE ME --"
});
var keystone_default = withAuth(
  (0, import_core2.config)({
    db: {
      provider: "postgresql",
      url: "postgresql://postgres:xS50dvTbdOnBuJl0@db.qbauqezuluaookfgactc.supabase.co:5432/postgres"
    },
    lists,
    session
  })
);
//# sourceMappingURL=config.js.map
