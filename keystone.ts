
// This file is what Keystone uses as the entry-point to your headless backend
// Keystone imports the default export of this file, expecting a Keystone configuration object
//   you can find out more at https://keystonejs.com/docs/apis/config

import { config, graphql } from '@keystone-6/core';
import { Context } from '.keystone/types';
import { lists } from './schema';
import { createAuth } from '@keystone-6/auth';
import { statelessSessions } from '@keystone-6/core/session';
/////////////////////////////////////////////////////////////////////


const { withAuth } = createAuth({
  listKey: 'User',
  identityField: 'name',
  secretField: 'password',
  sessionData: 'isAdmin',
  initFirstItem: {
    fields: ['name', 'password', 'age', 'post', 'isAdmin'],
  },
});

const session = statelessSessions({
  secret: '-- EXAMPLE COOKIE SECRET; CHANGE ME --',
});

/*KONFIG */
export default withAuth(
  config({
    db: {
      provider: 'postgresql',
      url: 'postgresql://postgres:xS50dvTbdOnBuJl0@db.qbauqezuluaookfgactc.supabase.co:5432/postgres',
    },
    lists,
    session,
  }));

