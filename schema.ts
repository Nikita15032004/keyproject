
import { list, } from '@keystone-6/core';
import { allowAll } from '@keystone-6/core/access';
import { createAuth } from '@keystone-6/auth';



import { text, relationship, password, timestamp, select, integer, checkbox } from '@keystone-6/core/fields';
import type { Lists } from '.keystone/types';
//////////////////////////////////////////////
type Session = {
  session?: {
    data: {
      id: string;
      isAdmin: boolean;
    }
  },
}

const filterPost = ({ session }: Session) => {
  if (session?.data.isAdmin) return true;
  return { isPublished: { equals: true } };
}
const isAdmin = ({ session, context, listKey, operation }: any) => session?.data.isAdmin;
export const lists: Lists = {
  User: list({
    access: {
      operation: {
        query: ({ session, context, listKey, operation }) => true,
        create: isAdmin,
        update: isAdmin,
        delete: isAdmin,
      }
    },

    fields: {
      name: text({
        validation: { isRequired: true }, isIndexed: 'unique',
        hooks: {
          validateInput: ({ addValidationError, resolvedData, fieldKey }) => {
            const name = resolvedData[fieldKey];
            if (name !== undefined && name !== null && !name.toString().includes('@')) {
              addValidationError(`The name address ${name} provided for the field ${fieldKey} must contain an '@' character`);
            }
          },
        },
      }),
      password: password(),
      age: integer({ validation: { isRequired: true }, defaultValue: 18 }),
      post: relationship({ ref: 'Post.author', many: false }),
      isAdmin: checkbox(),
    },
  }),

  Post: list({
    access: {
      operation: {
        query: ({ session, context, listKey, operation }) => true,
        create: isAdmin,
        update: isAdmin,
        delete: isAdmin,
      },
      filter: {
        query: filterPost
      }
    },
    hooks: {
      validateInput: ({ resolvedData, addValidationError }) => {
        const { author } = resolvedData;
        if (!author) {
          // We call addValidationError to indicate an invalid value.
          addValidationError('The author of a blog post cannot be the empty ');
        }
      }
    },
    fields: {
      title: text({ validation: { isRequired: true } }),
      isPublished: checkbox(),
      publishDate: timestamp(),
      author: relationship({ ref: 'User.post', many: false }),
    },
  }),
};
